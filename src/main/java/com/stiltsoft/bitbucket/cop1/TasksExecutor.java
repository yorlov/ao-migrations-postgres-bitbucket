package com.stiltsoft.bitbucket.cop1;

import com.atlassian.bitbucket.user.EscalatedSecurityContext;
import com.atlassian.bitbucket.user.SecurityService;
import com.atlassian.bitbucket.util.concurrent.ExecutorUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Nonnull;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;

import static com.atlassian.bitbucket.permission.Permission.REPO_READ;
import static com.atlassian.util.concurrent.ThreadFactories.namedThreadFactory;
import static java.util.concurrent.Executors.newFixedThreadPool;

@Component
public class TasksExecutor implements Executor, DisposableBean {

    private static final Logger log = LoggerFactory.getLogger(TasksExecutor.class);

    private ExecutorService executor;
    private EscalatedSecurityContext securityContext;

    @Autowired
    public TasksExecutor(SecurityService securityService) {
        executor = newFixedThreadPool(1, namedThreadFactory("cop1"));
        securityContext = securityService.withPermission(REPO_READ, "Community of Practice 1");
    }

    @Override
    public void execute(@Nonnull Runnable runnable) {
        executor.execute(() -> {
            try {
                securityContext.call(() -> {
                    runnable.run();
                    return null;
                });
            } catch (Exception e) {
                log.warn("Houston, we have a problem", e);
            }
        });
    }

    @Override
    public void destroy() {
        ExecutorUtils.shutdown(executor);
    }
}