package com.stiltsoft.bitbucket.cop1;

import com.atlassian.bitbucket.repository.RepositoryService;
import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.plugin.event.events.PluginEnabledEvent;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static com.atlassian.bitbucket.util.PageUtils.toStream;

@Component
public class TasksScheduler implements InitializingBean, DisposableBean {

    private static final Logger log = LoggerFactory.getLogger(TasksScheduler.class);

    private TasksService tasksService;
    private RepositoryService repositoryService;
    private EventPublisher eventPublisher;
    private TasksExecutor executor;

    @Autowired
    public TasksScheduler(TasksService tasksService,
                          RepositoryService repositoryService,
                          TasksExecutor executor,
                          @ComponentImport EventPublisher eventPublisher) {
        this.tasksService = tasksService;
        this.repositoryService = repositoryService;
        this.eventPublisher = eventPublisher;
        this.executor = executor;
    }

    @EventListener
    public void onPluginEnabled(PluginEnabledEvent event) {
        if (event.getPlugin().getKey().equals("com.stiltsoft.bitbucket.cop1")) {
            log.warn("Enjoy!");
            executor.execute(() -> toStream(repositoryService::findAll, 100).forEach(repository -> {
                tasksService.schedule(repository);
                log.warn("Scheduled {}", repository);
            }));
        }
    }

    @Override
    public void afterPropertiesSet() {
        eventPublisher.register(this);
    }

    @Override
    public void destroy() {
        eventPublisher.unregister(this);
    }
}