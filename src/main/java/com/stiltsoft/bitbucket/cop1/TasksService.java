package com.stiltsoft.bitbucket.cop1;

import com.atlassian.bitbucket.repository.Repository;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class TasksService {

    private TasksDao dao;
    private TransactionTemplate transactionTemplate;

    @Autowired
    public TasksService(TasksDao dao, @ComponentImport("salTransactionTemplate") TransactionTemplate transactionTemplate) {
        this.dao = dao;
        this.transactionTemplate = transactionTemplate;
    }

    public void schedule(Repository repository) {
        transactionTemplate.execute(() -> dao.createTask(repository.getId()));
    }
}