package com.stiltsoft.bitbucket.cop1;

import net.java.ao.Accessor;
import net.java.ao.Entity;
import net.java.ao.schema.Indexed;
import net.java.ao.schema.Table;

import static com.stiltsoft.bitbucket.cop1.AoTask.*;

@Table(TABLE_NAME)
public interface AoTask extends Entity {

    String TABLE_NAME = "TASKS";
    String REPOSITORY_COLUMN = "REPOSITORY";
    String CREATED_AT_COLUMN = "CREATED_AT";

    @Accessor(REPOSITORY_COLUMN)
    @Indexed
    int getRepository();

    @Accessor(CREATED_AT_COLUMN)
    long getCreatedAt();

}