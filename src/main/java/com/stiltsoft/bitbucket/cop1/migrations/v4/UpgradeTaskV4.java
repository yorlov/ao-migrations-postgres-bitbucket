package com.stiltsoft.bitbucket.cop1.migrations.v4;

import com.stiltsoft.bitbucket.cop1.migrations.AbstractMigrationTask;
import com.stiltsoft.bitbucket.cop1.migrations.Versions;

public class UpgradeTaskV4 extends AbstractMigrationTask {

    public UpgradeTaskV4() {
        super(Versions.V3, Versions.V4, AoTaskV4.class);
    }
}