package com.stiltsoft.bitbucket.cop1.migrations.v5;

import net.java.ao.Entity;
import net.java.ao.schema.Indexed;
import net.java.ao.schema.Table;

import static com.stiltsoft.bitbucket.cop1.AoTask.TABLE_NAME;

@Table(TABLE_NAME)
public interface AoTaskV5 extends Entity {

    @Indexed
    int getRepository();

    long getCreatedAt();

    String getV5();
}