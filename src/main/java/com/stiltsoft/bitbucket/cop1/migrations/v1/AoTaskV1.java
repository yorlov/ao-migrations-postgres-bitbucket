package com.stiltsoft.bitbucket.cop1.migrations.v1;

import net.java.ao.Entity;
import net.java.ao.schema.Indexed;
import net.java.ao.schema.Table;

import static com.stiltsoft.bitbucket.cop1.AoTask.TABLE_NAME;

@Table(TABLE_NAME)
public interface AoTaskV1 extends Entity {

    @Indexed
    int getRepository();

    long getCreatedAt();

    String getV1();
}