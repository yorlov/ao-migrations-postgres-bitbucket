package com.stiltsoft.bitbucket.cop1.migrations.v2;

import com.stiltsoft.bitbucket.cop1.migrations.AbstractMigrationTask;
import com.stiltsoft.bitbucket.cop1.migrations.Versions;

public class UpgradeTaskV2 extends AbstractMigrationTask {

    public UpgradeTaskV2() {
        super(Versions.V1, Versions.V2, AoTaskV2.class);
    }
}