package com.stiltsoft.bitbucket.cop1.migrations.v5;

import com.stiltsoft.bitbucket.cop1.migrations.AbstractMigrationTask;
import com.stiltsoft.bitbucket.cop1.migrations.Versions;

public class UpgradeTaskV5 extends AbstractMigrationTask {

    public UpgradeTaskV5() {
        super(Versions.V4, Versions.V5, AoTaskV5.class);
    }
}