package com.stiltsoft.bitbucket.cop1.migrations.v1;

import com.stiltsoft.bitbucket.cop1.migrations.AbstractMigrationTask;
import com.stiltsoft.bitbucket.cop1.migrations.Versions;

public class UpgradeTaskV1 extends AbstractMigrationTask {

    public UpgradeTaskV1() {
        super(Versions.V0, Versions.V1, AoTaskV1.class);
    }
}