package com.stiltsoft.bitbucket.cop1.migrations.v3;

import com.stiltsoft.bitbucket.cop1.migrations.AbstractMigrationTask;
import com.stiltsoft.bitbucket.cop1.migrations.Versions;

public class UpgradeTaskV3 extends AbstractMigrationTask {

    public UpgradeTaskV3() {
        super(Versions.V2, Versions.V3, AoTaskV3.class);
    }
}