package com.stiltsoft.bitbucket.cop1.migrations;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.activeobjects.external.ActiveObjectsUpgradeTask;
import com.atlassian.activeobjects.external.ModelVersion;
import net.java.ao.RawEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class AbstractMigrationTask implements ActiveObjectsUpgradeTask {

    private static final Logger log = LoggerFactory.getLogger(AbstractMigrationTask.class);

    private ModelVersion prev;
    private ModelVersion next;
    private Class<? extends RawEntity<?>> entity;

    public AbstractMigrationTask(int prev, int next, Class<? extends RawEntity<?>> entity) {
        this.prev = ModelVersion.valueOf(String.valueOf(prev));
        this.next = ModelVersion.valueOf(String.valueOf(next));
        this.entity = entity;
    }

    @Override
    public ModelVersion getModelVersion() {
        return next;
    }

    @Override
    public void upgrade(ModelVersion modelVersion, ActiveObjects ao) {
        if (modelVersion.isSame(prev)) {
            log.warn("Migrate to version {}", next);
            ao.migrateDestructively(entity);
        }
    }
}